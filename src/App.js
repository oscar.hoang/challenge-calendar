import React, { useState, useEffect } from 'react';
import './App.css';

import Calendar from './components/Calendar';
import Note from './components/Note';
import moment from 'moment';

const App = () => {
  const { note, setNote } = useState('');
  const { selectedDate, setSelectedDate } = useState('');
  const { notes, setNotes } = useState({});

  useEffect(() => {
    // TO-DO: look through object for date that matches selected date
    // TO-DO: if match update note. else add new note.
  }, [note]);

  const onNoteInputChange = (e) => {
    console.log(e.target.value);
    setNote(e.target.value);
    // TO-DO: method takes new note input, changes note state and adds this note for current date inside notes object
  }

  const onDayClick = (selectedDate) => {
    // TO-DO: method takes selected date string in DD-MM-YYYY and changes selectedDate and note accordingly
  }


  return (
    <div className="App">
      <Calendar
        onDayClick={onDayClick}
        selectedDate={selectedDate} />
      <Note 
        onNoteInputChange={onNoteInputChange}
        note={note} />
    </div>
  );
}

export default App;