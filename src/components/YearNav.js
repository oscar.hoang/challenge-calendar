import React, { Component } from 'react'

export default class YearNav extends Component {
  state = {
    showYearNav: false
  }

  year = () => {
    // TO-DO: method returns currently selected year
  }

  showYearEditor = () => {
    // TO-DO: method makes input field visible on double click of year label
  }

  onKeyUpYear = (e) => {
    if ((e.which === 13 || e.which === 27) && this.props.isValidYear(e.target.value)) {
      this.props.onYearChange(e.target.value);
      this.setState({
        showYearNav: false
      })
    }
  }

  isValidYear = (inputYear) => {
    // TO-DO: method validates year input
  }

  onYearChange = (e) => {
    this.props.onYearChange(e, e.target.value);
  }

  render() {
    return (
      this.state.showYearNav ?
      <input defaultValue = {''}
             className="editor-year"
             onKeyUp= {(e) => this.onKeyUpYear(e)}
             onChange = {(e) => this.onYearChange(e)}
             type="number"
             placeholder="year"/>
      :
      <span className="label-year"
            onDoubleClick={(e)=> { this.showYearEditor()}}>
            {this.year()}
      </span>
    );
  }
}
