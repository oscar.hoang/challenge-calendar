import React from 'react'

const Note = ({ note, onNoteInputChange }) => {
  return (
    <div>
      <textarea
        value={note}
        onChange={(e) => onNoteInputChange(e)}
        placeholder="Enter your note...">
      </textarea>
    </div>
  )
}

export default Note;
