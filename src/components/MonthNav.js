import React, { Component } from 'react'
import SelectMonth from './SelectMonth';
import moment from 'moment';

export default class MonthNav extends Component {
  state = {
    showMonthPopup: false
  }

  months = moment.months();

  onToggleDropdown = (e) => {
    // TO-DO: method toggles showMonthPopup flag when user clicks on label
  }

  month = () => {
    // TO-DO: method returns current month name so it renders in DOM
  }

  render() {
    return (
      <span className="label-month"
        onClick={() => {}}>

        {this.state.showMonthPopup &&
          <SelectMonth data={this.months} 
                       onMonthChange={this.props.onMonthChange}/>
        }
      </span>
    )
  }
}
